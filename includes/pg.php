<?php
class psql {
    public $db;
    function __construct() {
        $this->db = $this->conn();
    }
    public function conn()
    {
        return pg_connect("host=".DB_HOST." dbname=".DB_NAME." user=".DB_USER." password=".DB_PASS." port=".DB_PORT);
    }
    public function disconn()
    {
        return pg_close($this->db);
    }
    public function get_data($q)
    {
        if($q!=NULL)
        {
            $db=$this->db;
            $res = pg_query($db,$q);
            if($res!=NULL)
            {
                $n = 0;
                $tab = array();
                while ($line = pg_fetch_array($res, null, PGSQL_ASSOC))
                {
                    $tab[$n] = $line;
                    $n++;
                }
                pg_free_result($res);
                return $tab;
            }
        }
        return NULL;
    }
}
?>